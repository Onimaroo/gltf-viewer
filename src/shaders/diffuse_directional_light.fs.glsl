#version 330 core

#define M_PI 3.1415926538

in vec3 vViewSpacePosition; // Position du sommet transformé dans l'espace View
in vec3 vViewSpaceNormal; // Normale du sommet transformé dans l'espace View
in vec2 vTexCoords; // Coordonnées de texture du sommet

//uniform sampler2D uTexture;
//uniform sampler2D uTexture2;

vec3 viewDirection = -vViewSpacePosition;
uniform vec3 uLightIntensity;
uniform vec3 uLightDirection;

out vec3 fFragColor;

vec3 radiance(vec3 brdf, vec3 normal_vs, vec3 light_direction) {

	return brdf * uLightIntensity * dot(normal_vs, light_direction);
}

void main() {
    vec3 brdf = vec3(1 / M_PI, 1 / M_PI, 1 / M_PI);
    fFragColor = radiance(brdf, normalize(vViewSpaceNormal), normalize(uLightDirection));
};