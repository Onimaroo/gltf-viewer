#include "ViewerApplication.hpp"

#include <iostream>
#include <numeric>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/io.hpp>

#include "utils/cameras.hpp"
#include "utils/images.hpp"

#include <stb_image_write.h>
#include <tiny_gltf.h>

void keyCallback(
    GLFWwindow *window, int key, int scancode, int action, int mods)
{
  if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) {
    glfwSetWindowShouldClose(window, 1);
  }
}

int ViewerApplication::run()
{
  // Loader shaders
  const auto glslProgram = compileProgram({m_ShadersRootPath / m_vertexShader,
      m_ShadersRootPath / m_fragmentShader});

  const auto modelViewProjMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uModelViewProjMatrix");
  const auto modelViewMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uModelViewMatrix");
  const auto normalMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uNormalMatrix");

  const auto uLightDirectionLocation =
      glGetUniformLocation(glslProgram.glId(), "uLightDirection");
  const auto uLightIntensityLocation =
      glGetUniformLocation(glslProgram.glId(), "uLightIntensity");

  const auto uBaseColorTextureLocation =
      glGetUniformLocation(glslProgram.glId(), "uBaseColorTexture");
  const auto uBaseColorFactorLocation =
      glGetUniformLocation(glslProgram.glId(), "uBaseColorFactor");

  const auto uMetallicFactorLocation =
      glGetUniformLocation(glslProgram.glId(), "uMetallicFactor");
  const auto uRoughnessFactorLocation =
      glGetUniformLocation(glslProgram.glId(), "uRoughnessFactor");
  const auto uMetallicRoughnessTextureLocation =
      glGetUniformLocation(glslProgram.glId(), "uMetallicRoughnessTexture");

  const auto uEmissiveFactorLocation =
      glGetUniformLocation(glslProgram.glId(), "uEmissiveFactor");
  const auto uEmissiveTextureLocation =
      glGetUniformLocation(glslProgram.glId(), "uEmissiveTexture");

  const auto uOcclusionTextureLocation =
      glGetUniformLocation(glslProgram.glId(), "uOcclusionTexture");
  const auto uOcclusionStrengthLocation =
      glGetUniformLocation(glslProgram.glId(), "uOcclusionStrength");
  const auto uOcclusionBooleanLocation =
      glGetUniformLocation(glslProgram.glId(), "uOcclusionBoolean");

  
  auto lightDirection = glm::vec3(1, 1, 1);
  auto lightIntensity = glm::vec3(1, 1, 1);

  tinygltf::Model model;

  // TODO Loading the glTF file

  loadGltfFile(model);

    // Build projection matrix

  glm::vec3 bboxMin, bboxMax;

  computeSceneBounds(model, bboxMin, bboxMax);

  const auto diagonalVector = bboxMax - bboxMin;

  float maxDistance;

  if(glm::length(diagonalVector) != 0) {
    maxDistance = glm::length(diagonalVector);
  }
  else {
    maxDistance = 100.f;
  }

  const auto projMatrix = glm::perspective(70.f, float(m_nWindowWidth) / m_nWindowHeight, 0.001f * maxDistance, 1.5f * maxDistance);

  // TODO Implement a new CameraController model and use it instead. Propose the
  // choice from the GUI
  std::unique_ptr<CameraController> cameraController = std::make_unique<FirstPersonCameraController>(m_GLFWHandle.window(), 0.8f * maxDistance);

  if (m_hasUserCamera) {
    cameraController->setCamera(m_userCamera);
  } else {

    // TODO Use scene bounds to compute a better default camera

    const auto center = 0.5f * (bboxMax + bboxMin);

    // cameraController.setCamera(Camera{center, center + diagonalVector, glm::vec3(0, 1, 0)});

    const auto up = glm::vec3(0, 1, 0);

    glm::vec3 eye;

    if(diagonalVector.z > 0) {
      eye = center + diagonalVector;
    }
    else {
      eye = center + 2.f * glm::cross(diagonalVector, up);
    }

    cameraController->setCamera(Camera{eye, center, up});
  }

  const auto textureObjects = createTextureObjects(model); 

  GLuint whiteTexture = 0;
  float white[] = {1, 1, 1, 1};

  glGenTextures(1, &whiteTexture);
  glBindTexture(GL_TEXTURE_2D, whiteTexture);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1, 1, 0, GL_RGBA, GL_FLOAT, white);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_REPEAT);

  glBindTexture(GL_TEXTURE_2D, 0);


  // TODO Creation of Buffer Objects

  std::vector<GLuint> bufferObjects = ViewerApplication::createBufferObjects(model);

  // TODO Creation of Vertex Array Objects
  std::vector<VaoRange> meshIndexToVaoRange;
  const auto vertexArrayObject = ViewerApplication::createVertexArrayObjects(model, bufferObjects, meshIndexToVaoRange);

  // Setup OpenGL state for rendering
  glEnable(GL_DEPTH_TEST);
  glslProgram.use();

  bool lightCondition = false;
  bool occlusionCondition = true;

  const auto bindMaterial = [&](const auto materialIndex) {
    if (materialIndex >= 0) { // only valid is materialIndex >= 0
      const auto &material = model.materials[materialIndex];
      const auto &pbrMetallicRoughness = material.pbrMetallicRoughness;

      if (uBaseColorFactorLocation >= 0) {
        glUniform4f(uBaseColorFactorLocation,
          (float)pbrMetallicRoughness.baseColorFactor[0],
          (float)pbrMetallicRoughness.baseColorFactor[1],
          (float)pbrMetallicRoughness.baseColorFactor[2],
          (float)pbrMetallicRoughness.baseColorFactor[3]);
      }

      if (uBaseColorTextureLocation >= 0) {
        auto textureObject = whiteTexture;
        if (pbrMetallicRoughness.baseColorTexture.index >= 0) {
          const auto &texture = model.textures[pbrMetallicRoughness.baseColorTexture.index];
          if (texture.source >= 0) {
            textureObject = textureObjects[texture.source];
          }
        }

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        glUniform1i(uBaseColorTextureLocation, 0);
      }

      if(uMetallicFactorLocation >= 0) {
        glUniform1f(uMetallicFactorLocation, (float)pbrMetallicRoughness.metallicFactor);
      }

      if(uRoughnessFactorLocation >= 0) {
        glUniform1f(uRoughnessFactorLocation, (float)pbrMetallicRoughness.roughnessFactor);
      }

      if (uMetallicRoughnessTextureLocation >= 0) {
        auto textureObject = 0;
        if (pbrMetallicRoughness.metallicRoughnessTexture.index >= 0) {
          const auto &texture = model.textures[pbrMetallicRoughness.metallicRoughnessTexture.index];
          if (texture.source >= 0) {
            textureObject = textureObjects[texture.source];
          }
        }

        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        glUniform1i(uMetallicRoughnessTextureLocation, 1);
      }
      if(uEmissiveFactorLocation >= 0) {
        glUniform3f(uEmissiveFactorLocation, (float)material.emissiveFactor[0], (float)material.emissiveFactor[1], (float)material.emissiveFactor[2]);
      }

      if (uEmissiveTextureLocation >= 0) {
        auto textureObject = 0;
        if (material.emissiveTexture.index >= 0) {
          const auto &texture = model.textures[material.emissiveTexture.index];
          if (texture.source >= 0) {
            textureObject = textureObjects[texture.source];
          }
        }

        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        glUniform1i(uEmissiveTextureLocation, 2);
      }
      if (uOcclusionStrengthLocation >= 0) {
        glUniform1f(uOcclusionStrengthLocation, (float)material.occlusionTexture.strength);
      }
      if (uOcclusionTextureLocation >= 0) {
        auto textureObject = whiteTexture;
        if (material.occlusionTexture.index >= 0) {
          const auto &texture = model.textures[material.occlusionTexture.index];
          if (texture.source >= 0) {
            textureObject = textureObjects[texture.source];
          }
        }

        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        glUniform1i(uOcclusionTextureLocation, 3);
      }

    }
    else {

      if (uBaseColorFactorLocation >= 0) {
        glUniform4f(uBaseColorFactorLocation, 1, 1, 1, 1);
      }
      if(uMetallicFactorLocation >= 0) {
        glUniform1f(uMetallicFactorLocation, 1);
      }
      if(uRoughnessFactorLocation >= 0) {
        glUniform1f(uRoughnessFactorLocation, 1);
      }
      if(uEmissiveFactorLocation >= 0) {
        glUniform3f(uEmissiveFactorLocation, 0, 0, 0);
      }
      if(uOcclusionStrengthLocation >= 0) {
        glUniform1f(uOcclusionStrengthLocation, 0);
      }
      if (uBaseColorTextureLocation >= 0) {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, whiteTexture);
        glUniform1i(uBaseColorTextureLocation, 0);
      }
      if (uMetallicRoughnessTextureLocation >= 0) {
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, 0);
        glUniform1i(uMetallicRoughnessTextureLocation, 1);
      }
      if (uEmissiveTextureLocation >= 0) {
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, 0);
        glUniform1i(uEmissiveTextureLocation, 2);
      }
      if (uOcclusionTextureLocation >= 0) {
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, 0);
        glUniform1i(uOcclusionTextureLocation, 3);
      }
    }
  };

  // Lambda function to draw the scene
  const auto drawScene = [&](const Camera &camera) {
    glViewport(0, 0, m_nWindowWidth, m_nWindowHeight);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    const auto viewMatrix = camera.getViewMatrix();

    // Envoi des paramètres aux shaders
    if(uLightDirectionLocation >= 0) {
      if (lightCondition) {
        glUniform3f(uLightDirectionLocation, 0, 0, 1);
      } 
      else {
        auto uLightDirectionToViewSpace = glm::normalize(glm::vec3(viewMatrix * glm::vec4(lightDirection, 0)));
        glUniform3f(uLightDirectionLocation, uLightDirectionToViewSpace[0], uLightDirectionToViewSpace[1], uLightDirectionToViewSpace[2]);
      }
    }

    if(uLightIntensityLocation >= 0) {
        glUniform3f(uLightIntensityLocation, lightIntensity[0], lightIntensity[1], lightIntensity[2]);
    }

    if(uOcclusionBooleanLocation >= 0) {
      glUniform1i(uOcclusionBooleanLocation, occlusionCondition);
    }

    // The recursive function that should draw a node
    // We use a std::function because a simple lambda cannot be recursive
    const std::function<void(int, const glm::mat4 &)> drawNode =
        [&](int nodeIdx, const glm::mat4 &parentMatrix) {
          const tinygltf::Node node = model.nodes[nodeIdx];
          const glm::mat4 modelMatrix = getLocalToWorldMatrix(node, parentMatrix);
          if(node.mesh >= 0) {
            const glm::mat4 modelViewMatrix = viewMatrix * modelMatrix;
            const glm::mat4 modelViewProjectionMatrix = projMatrix * modelViewMatrix;
            const glm::mat4 normalMatrix = glm::transpose(glm::inverse(modelViewMatrix));

            glUniformMatrix4fv(modelViewMatrixLocation, 1, GL_FALSE, glm::value_ptr(modelViewMatrix));
            glUniformMatrix4fv(modelViewProjMatrixLocation, 1, GL_FALSE, glm::value_ptr(modelViewProjectionMatrix));
            glUniformMatrix4fv(normalMatrixLocation, 1, GL_FALSE, glm::value_ptr(normalMatrix));

            auto &mesh = model.meshes[node.mesh];
            auto &meshVaoRange = meshIndexToVaoRange[node.mesh];

            for (size_t primitiveIdx = 0; primitiveIdx < mesh.primitives.size(); ++primitiveIdx) {
              auto primitiveVAO = vertexArrayObject[meshVaoRange.begin + primitiveIdx];
              auto &currentPrimitive = mesh.primitives[primitiveIdx];
              bindMaterial(currentPrimitive.material);
              glBindVertexArray(primitiveVAO);
              if (currentPrimitive.indices >= 0) {
                // First case 
                auto &firstAccessor = model.accessors[currentPrimitive.indices];
                auto &bufferView = model.bufferViews[firstAccessor.bufferView];

                const auto byteOffset = firstAccessor.byteOffset + bufferView.byteOffset;
                glDrawElements(currentPrimitive.mode, GLsizei(firstAccessor.count), firstAccessor.componentType, (const GLvoid*)byteOffset);

              }
              else {
                // Second case
                const auto accessorIdx = (*begin(currentPrimitive.attributes)).second;
                const auto &secondAccessor = model.accessors[accessorIdx];

                glDrawArrays(currentPrimitive.mode, 0, secondAccessor.count);

              }
            }
          }
          for (int children : node.children) {
            drawNode(children, modelMatrix);
          }

        };

    // Draw the scene referenced by gltf file
    if (model.defaultScene >= 0) {
      for(int node : model.scenes[model.defaultScene].nodes) {
        drawNode(node, glm::mat4(1));
      }
    }
  };

  if(!m_OutputPath.empty()) {
    std::vector<unsigned char> pixels(m_nWindowWidth * m_nWindowHeight * 3);
    renderToImage(m_nWindowWidth, m_nWindowHeight, 3, pixels.data(), [&]() {
      drawScene(cameraController->getCamera());
    });

    flipImageYAxis(m_nWindowWidth, m_nWindowHeight, 3, pixels.data());

    const auto strPath = m_OutputPath.string();
    stbi_write_png(strPath.c_str(), m_nWindowWidth, m_nWindowHeight, 3, pixels.data(), 0);

    return 0;
  }

  // Loop until the user closes the window
  for (auto iterationCount = 0u; !m_GLFWHandle.shouldClose();
       ++iterationCount) {
    const auto seconds = glfwGetTime();

    const auto camera = cameraController->getCamera();
    drawScene(camera);

    // GUI code:
    imguiNewFrame();

    {
      ImGui::Begin("GUI");
      ImGui::Text("Application average %.3f ms/frame (%.1f FPS)",
          1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
      if (ImGui::CollapsingHeader("Camera", ImGuiTreeNodeFlags_DefaultOpen)) {
        ImGui::Text("eye: %.3f %.3f %.3f", camera.eye().x, camera.eye().y,
            camera.eye().z);
        ImGui::Text("center: %.3f %.3f %.3f", camera.center().x,
            camera.center().y, camera.center().z);
        ImGui::Text(
            "up: %.3f %.3f %.3f", camera.up().x, camera.up().y, camera.up().z);

        ImGui::Text("front: %.3f %.3f %.3f", camera.front().x, camera.front().y,
            camera.front().z);
        ImGui::Text("left: %.3f %.3f %.3f", camera.left().x, camera.left().y,
            camera.left().z);

        if (ImGui::Button("CLI camera args to clipboard")) {
          std::stringstream ss;
          ss << "--lookat " << camera.eye().x << "," << camera.eye().y << ","
             << camera.eye().z << "," << camera.center().x << ","
             << camera.center().y << "," << camera.center().z << ","
             << camera.up().x << "," << camera.up().y << "," << camera.up().z;
          const auto str = ss.str();
          glfwSetClipboardString(m_GLFWHandle.window(), str.c_str());
        }

        static int cameraControllerType = 0;
        const auto cameraControllerTypeChanged = ImGui::RadioButton("Default Camera", &cameraControllerType, 0) || ImGui::RadioButton("Trackball Camera", &cameraControllerType, 1);

        if (cameraControllerTypeChanged) {
          const auto currentCamera = cameraController->getCamera();
          if (cameraControllerType == 0) {
            cameraController = std::make_unique<FirstPersonCameraController>(m_GLFWHandle.window(), 0.8f * maxDistance);
          } 
          else {
            cameraController = std::make_unique<TrackballCameraController>(m_GLFWHandle.window(), 0.8f * maxDistance);
          }
          cameraController->setCamera(currentCamera);
        }
        
      }

      // Configuration du GUI pour la lumière
      if(ImGui::CollapsingHeader("Light", ImGuiTreeNodeFlags_DefaultOpen)) {
        static float lightTheta = 0.f;
        static float lightPhi = 0.f;

        if(ImGui::SliderFloat("Theta", &lightTheta, 0, glm::pi<float>()) || ImGui::SliderFloat("Phi", &lightTheta, 0, 2 * glm::pi<float>())) { // Gestion de la lumière directionnelle
          lightDirection = glm::vec3(glm::sin(lightTheta) * glm::cos(lightPhi), glm::cos(lightTheta), glm::sin(lightTheta) * glm::sin(lightPhi));
        }

        glm::vec3 lightColor(1.f, 1.f, 1.f);
        float lightIntensityFactor = 1.f;

        if (ImGui::ColorEdit3("Color", (float *)&lightColor) || ImGui::InputFloat("Intensity", &lightIntensityFactor)) { // Gestion des couleurs
          lightIntensity = lightColor * lightIntensityFactor;
        }

        ImGui::Checkbox("Lighting from camera", &lightCondition); // Condition pour changer la source d'émission de lumière.
        ImGui::Checkbox("Occlusion", &occlusionCondition); // Condition pour l'occlusion
  
      }
      ImGui::End();
    }

    imguiRenderFrame();

    glfwPollEvents(); // Poll for and process events

    auto ellapsedTime = glfwGetTime() - seconds;
    auto guiHasFocus =
        ImGui::GetIO().WantCaptureMouse || ImGui::GetIO().WantCaptureKeyboard;
    if (!guiHasFocus) {
      cameraController->update(float(ellapsedTime));
    }

    m_GLFWHandle.swapBuffers(); // Swap front and back buffers
  }

  // TODO clean up allocated GL data

  return 0;
}

ViewerApplication::ViewerApplication(const fs::path &appPath, uint32_t width,
    uint32_t height, const fs::path &gltfFile,
    const std::vector<float> &lookatArgs, const std::string &vertexShader,
    const std::string &fragmentShader, const fs::path &output) :
    m_nWindowWidth(width),
    m_nWindowHeight(height),
    m_AppPath{appPath},
    m_AppName{m_AppPath.stem().string()},
    m_ImGuiIniFilename{m_AppName + ".imgui.ini"},
    m_ShadersRootPath{m_AppPath.parent_path() / "shaders"},
    m_gltfFilePath{gltfFile},
    m_OutputPath{output}
{
  if (!lookatArgs.empty()) {
    m_hasUserCamera = true;
    m_userCamera =
        Camera{glm::vec3(lookatArgs[0], lookatArgs[1], lookatArgs[2]),
            glm::vec3(lookatArgs[3], lookatArgs[4], lookatArgs[5]),
            glm::vec3(lookatArgs[6], lookatArgs[7], lookatArgs[8])};
  }

  if (!vertexShader.empty()) {
    m_vertexShader = vertexShader;
  }

  if (!fragmentShader.empty()) {
    m_fragmentShader = fragmentShader;
  }

  ImGui::GetIO().IniFilename =
      m_ImGuiIniFilename.c_str(); // At exit, ImGUI will store its windows
                                  // positions in this file

  glfwSetKeyCallback(m_GLFWHandle.window(), keyCallback);

  printGLVersion();
}
