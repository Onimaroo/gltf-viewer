#pragma once

#include "utils/GLFWHandle.hpp"
#include "utils/cameras.hpp"
#include "utils/filesystem.hpp"
#include "utils/shaders.hpp"

#include "utils/gltf.hpp"

#include "tiny_gltf.h"

const GLuint VERTEX_ATTRIB_POSITION_IDX = 0;
const GLuint VERTEX_ATTRIB_NORMAL_IDX = 1;
const GLuint VERTEX_ATTRIB_TEXCOORD0_IDX = 2;

class ViewerApplication
{
private:
  // A range of indices in a vector containing Vertex Array Objects
  struct VaoRange
  {
    GLsizei begin; // Index of first element in vertexArrayObjects
    GLsizei count; // Number of elements in range
  };

  GLsizei m_nWindowWidth = 1280;
  GLsizei m_nWindowHeight = 720;

  const fs::path m_AppPath;
  const std::string m_AppName;
  const fs::path m_ShadersRootPath;

  fs::path m_gltfFilePath;
  std::string m_vertexShader = "forward.vs.glsl";
  // std::string m_fragmentShader = "diffuse_directional_light.fs.glsl";
  std::string m_fragmentShader = "pbr_directional_light.fs.glsl";

  bool m_hasUserCamera = false;
  Camera m_userCamera;

  fs::path m_OutputPath;

  // Order is important here, see comment below
  const std::string m_ImGuiIniFilename;
  // Last to be initialized, first to be destroyed:
  GLFWHandle m_GLFWHandle{int(m_nWindowWidth), int(m_nWindowHeight),
      "glTF Viewer",
      m_OutputPath.empty()}; // show the window only if m_OutputPath is empty
  /*
    ! THE ORDER OF DECLARATION OF MEMBER VARIABLES IS IMPORTANT !
    - m_ImGuiIniFilename.c_str() will be used by ImGUI in ImGui::Shutdown, which
    will be called in destructor of m_GLFWHandle. So we must declare
    m_ImGuiIniFilename before m_GLFWHandle so that m_ImGuiIniFilename
    destructor is called after.
    - m_GLFWHandle must be declared before the creation of any object managing
    OpenGL resources (e.g. GLProgram, GLShader) because it is responsible for
    the creation of a GLFW windows and thus a GL context which must exists
    before most of OpenGL function calls.
  */
public:
  ViewerApplication(const fs::path &appPath, uint32_t width, uint32_t height,
      const fs::path &gltfFile, const std::vector<float> &lookatArgs,
      const std::string &vertexShader, const std::string &fragmentShader,
      const fs::path &output);

  bool loadGltfFile(tinygltf::Model &model)
  {
    std::string err;
    std::string warn;
    tinygltf::TinyGLTF loader;

    bool ret =
        loader.LoadASCIIFromFile(&model, &err, &warn, m_gltfFilePath.string());

    if (!warn.empty()) {
      printf("Warn: %s\n", warn.c_str());
    }

    if (!err.empty()) {
      printf("Err: %s\n", err.c_str());
    }

    if (!ret) {
      printf("Failed to parse glTF\n");
      return -1;
    }

    return ret;
  }

  std::vector<GLuint> createBufferObjects(const tinygltf::Model &model)
  {
    std::vector<GLuint> bufferObjects(model.buffers.size(), 0);
    glGenBuffers(model.buffers.size(), bufferObjects.data());
    for (size_t bufferIdx = 0; bufferIdx < model.buffers.size(); ++bufferIdx) {
      glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[bufferIdx]);
      glBufferStorage(GL_ARRAY_BUFFER, model.buffers[bufferIdx].data.size(),
          model.buffers[bufferIdx].data.data(), 0);
    }
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    return bufferObjects;
  }

  std::vector<GLuint> createVertexArrayObjects(const tinygltf::Model &model,
      const std::vector<GLuint> &bufferObjects,
      std::vector<VaoRange> &meshIndexToVaoRange)
  {

    std::vector<GLuint> vertexArrayObjects;
    meshIndexToVaoRange.resize(model.meshes.size());

    for (size_t meshIdx = 0; meshIdx < model.meshes.size(); ++meshIdx) {

      tinygltf::Primitive primitive;

      const auto vaoOffset = GLsizei(vertexArrayObjects.size());

      vertexArrayObjects.resize(vaoOffset + model.meshes[meshIdx].primitives.size());
      meshIndexToVaoRange.push_back(VaoRange{vaoOffset, (int) model.meshes[meshIdx].primitives.size()}); // Will be used during rendering
      
      glGenVertexArrays(model.meshes[meshIdx].primitives.size(), &vertexArrayObjects[vaoOffset]);

      for (size_t primitiveIdx = 0; primitiveIdx < model.meshes[meshIdx].primitives.size(); ++primitiveIdx) {
        glBindVertexArray(vertexArrayObjects[vaoOffset + primitiveIdx]);
        primitive = model.meshes[meshIdx].primitives[primitiveIdx];
        const auto iterator_position = primitive.attributes.find("POSITION");
        {
          if (iterator_position != end(primitive.attributes)) {
            const auto accessorIdx = (*iterator_position).second;
            const auto &accessor = model.accessors[accessorIdx];
            const auto &bufferView = model.bufferViews[accessor.bufferView];
            const auto bufferIdx = bufferView.buffer;

            const auto bufferObject = bufferObjects[bufferIdx];

            glEnableVertexAttribArray(VERTEX_ATTRIB_POSITION_IDX);
            assert(GL_ARRAY_BUFFER == bufferView.target);
            glBindBuffer(GL_ARRAY_BUFFER, bufferObject);

            const auto byteOffset = accessor.byteOffset + bufferView.byteOffset;
            glVertexAttribPointer(VERTEX_ATTRIB_POSITION_IDX, accessor.type, accessor.componentType, GL_FALSE, GLsizei(bufferView.byteStride), (const GLvoid *)byteOffset);
          }
          const auto iterator_normal = primitive.attributes.find("NORMAL");
          if (iterator_normal != end(primitive.attributes)) {
            const auto accessorIdx = (*iterator_normal).second;
            const auto &accessor = model.accessors[accessorIdx];
            const auto &bufferView = model.bufferViews[accessor.bufferView];
            const auto bufferIdx = bufferView.buffer;

            const auto bufferObject = bufferObjects[bufferIdx];

            glEnableVertexAttribArray(VERTEX_ATTRIB_NORMAL_IDX);
            assert(GL_ARRAY_BUFFER == bufferView.target);
            glBindBuffer(GL_ARRAY_BUFFER, bufferObject);

            const auto byteOffset = accessor.byteOffset + bufferView.byteOffset;
            glVertexAttribPointer(VERTEX_ATTRIB_NORMAL_IDX, accessor.type, accessor.componentType, GL_FALSE, GLsizei(bufferView.byteStride), (const GLvoid *)byteOffset);
          }
          const auto iterator_texcoord = primitive.attributes.find("TEXCOORD_0");
          if (iterator_texcoord != end(primitive.attributes)) {
            const auto accessorIdx = (*iterator_texcoord).second;
            const auto &accessor = model.accessors[accessorIdx];
            const auto &bufferView = model.bufferViews[accessor.bufferView];
            const auto bufferIdx = bufferView.buffer;

            const auto bufferObject = bufferObjects[bufferIdx];

            glEnableVertexAttribArray(VERTEX_ATTRIB_TEXCOORD0_IDX);
            assert(GL_ARRAY_BUFFER == bufferView.target);
            glBindBuffer(GL_ARRAY_BUFFER, bufferObject);

            const auto byteOffset = accessor.byteOffset + bufferView.byteOffset;
            glVertexAttribPointer(VERTEX_ATTRIB_TEXCOORD0_IDX, accessor.type, accessor.componentType, GL_FALSE, GLsizei(bufferView.byteStride), (const GLvoid *)byteOffset);
          }
        }
      
        if(primitive.indices >= 0) {
          const auto accessorIdx = primitive.indices;
          const auto &accessor = model.accessors[accessorIdx];
          const auto &bufferView = model.bufferViews[accessor.bufferView];
          const auto bufferIdx = bufferView.buffer;

          const auto bufferObject = bufferObjects[bufferIdx];
          glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferObject);
        }
      }
    }
    glBindVertexArray(0);

    std::clog << "Number of VAOs: " << vertexArrayObjects.size() << std::endl;
    
    return vertexArrayObjects;
  }

  std::vector<GLuint> createTextureObjects(const tinygltf::Model &model){
    std::vector<GLuint> textureObjects(model.textures.size(), 0);

    tinygltf::Sampler defaultSampler;
    defaultSampler.minFilter = GL_LINEAR;
    defaultSampler.magFilter = GL_LINEAR;
    defaultSampler.wrapS = GL_REPEAT;
    defaultSampler.wrapT = GL_REPEAT;
    defaultSampler.wrapR = GL_REPEAT;

    glActiveTexture(GL_TEXTURE0);

    glGenTextures(GLsizei(model.textures.size()), textureObjects.data());
    for (size_t i = 0; i < model.textures.size(); ++i) {

      // Here we assume a texture object has been created and bound to GL_TEXTURE_2D
      const auto &texture = model.textures[i]; // get i-th texture
      assert(texture.source >= 0);
      const auto &image = model.images[texture.source];

      // fill the texture object with the data from the image
      glBindTexture(GL_TEXTURE_2D, textureObjects[i]);
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image.width, image.height, 0, GL_RGBA, image.pixel_type, image.image.data());
      
      // Assume a texture object has been created and bound to GL_TEXTURE_2D
      const auto &sampler = texture.sampler >= 0 ? model.samplers[texture.sampler] : defaultSampler;
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
        sampler.minFilter != -1 ? sampler.minFilter : GL_LINEAR);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
        sampler.magFilter != -1 ? sampler.magFilter : GL_LINEAR);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, sampler.wrapS);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, sampler.wrapT);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, sampler.wrapR);

      if (sampler.minFilter == GL_NEAREST_MIPMAP_NEAREST ||
          sampler.minFilter == GL_NEAREST_MIPMAP_LINEAR ||
          sampler.minFilter == GL_LINEAR_MIPMAP_NEAREST ||
          sampler.minFilter == GL_LINEAR_MIPMAP_LINEAR) {
        glGenerateMipmap(GL_TEXTURE_2D);
      }
    }
    glBindTexture(GL_TEXTURE_2D, 0);

    return textureObjects;
  }

  int run();
};